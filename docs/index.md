# Welcome to the Automotive SIG

This site contains general information about the Automotive SIG, as well as
how to contribute to the repository and how to build and download images.

The Automotive SIG manages several artifacts:

* **Automotive Stream Distribution (AutoSD)**: This project is a binary
  distribution developed within the SIG that is a public,
  in-development preview of the upcoming Red Hat In-Vehicle
  Operating System (OS).
* **RPM repositories**: These are RPM repositories produced
  by the Automotive SIG to enhance AutoSD. New packages or features can be
  developed and hosted there to expand the capabilities of the
  AutoSD.
* **Sample images**: These are images built with [OSBuild](https://www.osbuild.org/)
  using packages from the AutoSD, the Automotive SIG repositories, or other
  sources. They are examples of how to use AutoSD.

For more information about the Automotive SIG charter, members, or goal, see the
[Automotive SIG page on the CentOS wiki](https://wiki.centos.org/SpecialInterestGroup/Automotive).

For Automotive SIG discussions, see [CentOS-Automotive-SIG mailing list](https://lists.centos.org/mailman/listinfo/centos-automotive-sig).
Or join the Matrix channel `#centos-automotive-sig`. For more information, see [centos-automotive-sig:fedoraproject.org](https://matrix.to/#/#centos-automotive-sig:fedoraproject.org).

## Automotive SIG repositories

Automotive SIG repositories include packages that are not
necessarily part of Red Hat In-Vehicle OS. These packages might be works in progress
that could land in AutoSD or Red Hat In-Vehicle OS.
Or the packages could be purely experimental--for research and
development--or integration work that remains outside of AutoSD.

All SIG members can request packages to be distributed through these
repositories, provided they meet [CentOS requirements for SIG](https://wiki.centos.org/SpecialInterestGroup#Requirements).

Browse the Automotive SIG repositories at:
[https://buildlogs.centos.org/9-stream/automotive/](https://buildlogs.centos.org/9-stream/automotive/).

### Downloading the repository

**Prerequisites**

* At least 1.6G of available disk space to accommodate
both `aarch64` and `x86_64`

!!! note

    The disk space requirement might change as the package set evolves.

**Procedure**

* Download a local copy of the repository:

```console
wget --recursive --no-parent -R "index.html*" 'https://autosd.sig.centos.org/AutoSD-9/nightly/repos/AutoSD/compose/AutoSD/'
```

!!! note

    The latest version of AutoSD is also available at
    [https://autosd.sig.centos.org](https://autosd.sig.centos.org).

## Sample images

!!! important

    Do not use sample images in production.

The [Automotive SIG](https://gitlab.com/centos/automotive/sample-images)
repository contains manifests for different operating systems and platforms.

The manifests are located in the `images` folder.

```console
    sample-images/
    └── images
```

For example:

```console
    sample-images/
    └── images
       ├── minimal.mpp.yml
       └── ...
```

The Automotive SIG uses [OSBuild](https://www.osbuild.org/) to build images.
For more information, see [Building images](building/index.md).

For information about pre-built images, see
[Downloading images produced by the Automotive SIG](download_images.md).
