# Customizing the OSbuild templates

There are three ways one can customize an OSbuild template:

- Manually edit the template
- Overriding variables on the CLI
- Overriding variables via a custom distro file

We will not be covering here the first method as this is the least sustainable,
changes made to the template would need to be rebased every time a change is
made to the repository.

First, let's discuss about these variables.

## Template variables

The manifests currently include a number of variables, which appear in different `mpp-vars`
sections of the OSbuild template manifests.

For example, a `mpp-vars` section may look like the following example:

```json
  "mpp-vars": {
    "rootfs_uuid": {"mpp-format-string": "{__import__('uuid').uuid1()}"},
    "bootfs_uuid": {"mpp-format-string": "{__import__('uuid').uuid1()}"},
    "rootfs_size": 4294967296,
    "homefs_size": {"mpp-format-string": "{rootfs_size}"}
  },
```

This example defines four variables: `rootfs_uuid`, `bootfs_uuid`, `rootfs_size`
and `homefs_size`.

- `rootfs_uuid` and `bootfs_uuid` dynamically generate when you run the equivalent of the following
  python code:

```python
import uuid
uuid.uuid1()
```

- `rootfs_size` is hardcoded to `4294967296` bytes.

- `homefs_size` is equal to `rootfs_size`.

## Overriding variables on the CLI

You can use the `DEFINES` variable to override variables in the manifest.
This variable contains a space-separated list of items in `variable=json-data` form.

To add extra packages and decrease the image_size to 3 GB:

```console
make cs9-qemu-minimal-regular.x86_64.qcow2 DEFINES='extra_rpms=["gdb","strace"] image_size=3489660928'
```

A Distro file normally defines these variables:

- `distro_name`: The name of the distro; default is `cs9`.
- `distro_version`: The version of the distro.
- `distro_baseurl`: The base URL of the distribution's repo; can be overridden to use a local mirror.
- `distro_module_id`: The is used for dnf modules; default `platform:el9`.
- `distro_repos`: The set of default repos for the distro.
- `distro_devel_repos`: The set of development repos for the distro.
- `distro_debug_repos`: The set of debug-info repos for the distro.

Some other variables you might want to override include:

- `extra_repos`: A list of extra yum repos to install rpms from;  defaults to empty.
- `extra_rpms`: List of additional RPMs installed in most images;  defaults to empty.
- `kernel_rpm`: The name of the kernel package used; defaults to `kernel-automotive`.
- `linux_firmware_rpm`: The name of the linux-firmware package used; defaults to `linux-firmware-automotive`.

- `osname`: The os name (used in ostree deployment); defaults to `centos`.
- `os_version`: The os version (use din ostree deployment); defaults to `$distro_version`
- `ostree_ref`: The name used for the ref when you commit to an `ostree` repo.
- `ostree_remote_name`: The name of the main ostree remote; defaults to `auto-sig`.
- `ostree_repo_url`: The URL of the main ostree remote; defaults to `http://10.0.2.100/`.

- `use_efi_runtime: If true, enable efi runtime services; defaults to`true`.
- `kernel_opts`: Base kernel options.
- `kernel_loglevel`: Kernel log level; defaults to 4.

- `root_password`: Password for root account; default `password`.
- `root_ssh_key`: Ssh key for root account; default empty.
- `guest_password`: Password for `guest` account; default `password`.

- `ssh_permit_root_login`: If true, enables ssh login for root account; defaults to `false`.
- `ssh_permit_password_auth`: If true, enables password authentication with ssh; defaults to `false`.

- `display_server`: The name of the display server (`wayland` or `xorg`); defaults to `wayland`.

- `image_size`: The total size of the disk image, in bytes as a string; defaults to 8 GB.
- `efipart_size`: The size of the EFI partition.
- `bootpart_size`: The size of the boot partition.
- `partition_label`: Either `gpt` (default) for gpt partition layout, or `dos` for dos partition layout (for rpi4).
- `static_uuids`: Set to true to use static default uuids for filesystems rather than generated. Default is `true`.
   If enabled, uses `rootfs_uuid`, `bootfs_uuid`, `parttab_uuid`, `efipart_uuid`,  `bootpart_uuid`, `rootpart_uuid`, and `luks_uuid` for specific values.

- `use_luks`: If true, encrypt the root fs with LUKS; default `false`.
- `luks_passphrase`: The passphrase used to encrypt the rootfs. Default `password`.
- `luks_auto_unlock`: If true, automatically unlock the encrypted rootfs on boot with a key-file.
   This is meant to be replaced later with a per-machine key. Default `false`.
- `luks_use_integrity`: If true, combine dm-integrity with encryption. Note: experimental due to low performance. Default `false`.

- `dracut_add_modules`: Dracut modules to add
- `dracut_omit_modules`: Dracut modules to omit
- `dracut_filesystem`: Dracut filesystems to install (defaults to `vfat` and `ext4`).
- `dracut_add_drivers`: Special drivers to install into dracut.
- `dracut_install`: Special files to copy into dracut.

## Overriding variables via a custom distro file

The simplest way to override variables using a customized file in the [`distro/`](
https://gitlab.com/CentOS/automotive/sample-images/-/tree/main/distro)
folder in the [sample-images](https://gitlab.com/CentOS/automotive/sample-images/)
repository.

As an example, we will create a customized image pulling two RPMs from the [EPEL](
https://docs.fedoraproject.org/en-US/epel/) repository.

### Create the custom file

```console
cp distro/cs9.ipp.yml distro/custom.ipp.yml
```

### Edit that custom file

Open `distro/custom.ipp.yml` in your favorite text editor and adjust it so that
the diff between `cs9.ipp.yml` and `custom.ipp.yml` is as follow:

```diff
--- distro/cs9.ipp.yml 2022-04-12 10:20:18.130917511 +0200
+++ distro/custom.ipp.yml 2022-04-22 16:18:11.203590871 +0200
@@ -10,6 +10,8 @@
     baseurl: $distro_baseurl/AppStream/$arch/os/
   - id: automotive
     baseurl: https://buildlogs.centos.org/9-stream/automotive/$arch/packages-main/
+  - id: epel-9
+    baseurl: https://dl.fedoraproject.org/pub/epel/9/Everything/$arch/
   distro_devel_repos:
   - id: crb
     baseurl: $distro_baseurl/CRB/$arch/os/
@@ -21,3 +23,7 @@
   - id: crb-debug
     baseurl: $distro_baseurl/CRB/$arch/debug/tree/
   distro_module_id: platform:el9
+  extra_rpms:
+  - gnuplot
+  - python3-zmq
+  root_password: $6$3c5hyALn6Ge/XXwL$Qr961XQbJeCC/aRLNd4CagSRUa/x7tOoG2MrnKpPCrZxoNYSZ4N3bfFQO99A6vQUuAWOOoUclqxPa2DJ0Ylg90
```

As you can see, we've added a new repo named `epel-9` with its `baseurl`. Lower
down, we have defined a `extra_rpms` variable corresponding to a list with two packages:
`gnuplot` and `python3-zmq`. We have also changed the root password from `password` to
`password2` (the `guest` password remaining the same).

!!! note
    To encrypt the password you can use: `mkpasswd --method=SHA-512 --stdin`

### Create the image

Now that we have this `distro/custom.ipp.yml` created, if we ran `make help`
we will this in its output:

```console
...
custom-qemu-container-regular.x86_64.[img,qcow2,oci.tar,repo,rootfs,ext4,tar]
custom-qemu-container-ostree.x86_64.[img,qcow2,oci.tar,repo,rootfs,ext4,tar]
custom-qemu-container-direct.x86_64.[img,qcow2,oci.tar,repo,rootfs,ext4,tar]
custom-qemu-developer-regular.x86_64.[img,qcow2,oci.tar,repo,rootfs,ext4,tar]
...
```

So we can create the image of our choice:

```console
make custom-qemu-developer-regular.x86_64.qcow2
```

### Boot and check

Once the image has been built, we can easily run it using `runvm`:

```console
./runvm custom-qemu-developer-direct.x86_64.qcow2
```

We can then log into the image with the `root` user, using `password2` as password
and check that the two new packages were installed:

```console
[root@localhost ~]# rpm -q python3-zmq gnuplot
python3-zmq-22.3.0-2.el9.x86_64
gnuplot-5.4.3-2.el9.x86_64
```
