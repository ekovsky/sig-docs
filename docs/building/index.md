# Building images

The Automotive SIG uses [OSBuild](https://www.osbuild.org/) as the tool to build
its images on CentOS Stream, Fedora, and RHEL hosts, with the option to build immutable images using [OSTree](https://ostreedev.github.io/ostree/introduction/).

## Prerequisites

- **No cross-compilation**: To build AArch64 or x86_64 images, you must run OSBuild on the respective systems.
  Some options for AArch64 hosting include Raspberry Pi 4 or qemu on Linux or macOS.
  For more information, see [AutoSD system running on a Raspberry Pi 4](autosd_pi4.md) or [AutoSD system running on qemu](autosd_qemu.md).

- **A subscribed RHEL system**: To build RHEL images, you must have access to a subscribed RHEL system to access to the entitlements.
  RHEL images can only be built on subscribed RHEL systems.

## Using the Automotive OSBuild Container Image

The Automotive SIG maintains a container image that you can use to build Automotive images for containerized enviroments.
The image is maintained in the
[quay.io/centos-sig-automotive/automotive-osbuild](https://quay.io/repository/centos-sig-automotive/automotive-osbuild?tab=tags) repo.

**Procedure**

1. Install `podman`:

```console
sudo dnf install podman
```

2. Build an image from the `sample-images` git repository:

```console
git clone https://gitlab.com/CentOS/automotive/sample-images.git
sudo sample-images/auto-image-builder.sh cs9-qemu-minimal-ostree.x86_64.qcow2
```

### Building on aarch64 macOS using podman 5

On macOS podman is using Apple's virtualization framework and can't use a volume shared from host for building directly.
You have to build from a writable filesystem e.g. /root.

1. Install `podman` from homebrew

```console
brew install podman
```

2. Initialize podman machine with rootful support for osbuild

```console
podman machine init --rootful
podman machine start
```

3. Launch podman with sharing a volume from machine's host

```console
sudo podman run -it -v /dev:/dev -v /root:/root -v $PWD:/host --rm --privileged --pull=newer --security-opt label=type:unconfined_t quay.io/centos-sig-automotive/automotive-osbuild
```

4. Copy the osbuild sources or create a new checkout in a writeable directory

```console
cp -r /host /root/sample-images/
```

or

```console
git clone https://gitlab.com/CentOS/automotive/sample-images.git /root/sample-images
```

5. Build in /root and copy back to /host dir

```console
cd /root/sample-images
make cs9-qemu-minimal-ostree.aarch64.qcow2
mv cs9-qemu-minimal-ostree.aarch64.qcow2 /host
```

!!! important

    The git repository must exist on the host, because some commands cannot work from the containers `overlayfs` filesystem.
    For more information about the container, see the
    [automotive-osbuild](https://gitlab.com/CentOS/automotive/container-images/automotive-osbuild) repository.

## Installing osbuild

To configure your system, install osbuild and other related tools.

!!! note

    The Automotive SIG uses newer versions of these tools than what are generally available in the distributions, so you must configure a custom repository.

### CentOS Stream 8 only

**Procedure**

- Enable the osbuild repo:

    ```console
    dnf copr enable @osbuild/osbuild-stable centos-stream-8-`uname -p`
    ```

### CentOS Stream 8/9, Fedora, or RHEL 8/9

**Procedure**

1. Enable the osbuild repo:

    ```console
    dnf copr enable @osbuild/osbuild-stable
    dnf copr enable @centos-automotive-sig/osbuild-auto
    ```

2. Install osbuild and related packages:

    ```console
    dnf install osbuild osbuild-auto osbuild-ostree osbuild-tools
    ```

## Finding a manifest

All manifests exist in the [Automotive SIG sample-images repository](https://gitlab.com/CentOS/automotive/sample-images).

**Procedure**

1. Clone the repository to download all manifests:

    ```console
    git clone https://gitlab.com/CentOS/automotive/sample-images.git
    ```

2. Primary image manifests are in the `images` folder.

    ```console
    images
    ├── composefs.mpp.yml
    ├── container.mpp.yml
    ├── containerperf.mpp.yml
    ├── developer.mpp.yml
    ├── directboot.csv
    ├── encrypted.mpp.yml
    ├── gadget.mpp.yml
    ├── minimal.mpp.yml
    ├── ocibased.mpp.yml
    ├── ocp.mpp.yml
    ├── qmcontainer.mpp.yml
    └── upgrade-demo.mpp.yml
    ```

## Building the image

You can build an image manually or by using makefile.

### Building the image manually

Building an image manually is a two-step process.

**Procedure**

1. Pass the selected YAML manifest and some options to the `osbuild-mpp` tool, for example:

   ```console
   osbuild-mpp images/minimal.mpp.yml /tmp/output.json -I . -D arch=\"aarch64\"
   ```

   This example command applies the options and resolves the package names against the repositories
   used to produce a JSON manifest with fully resolved versions of all packages.
   This JSON file is fully self contained and produces reproducible builds.

   The manifest has multiple options that can affect how the manifest is preprocessed, for example:

   - `image_type` specifies whether the system is _ostree_ or _regular (dnf)_ based.
   - `extra_rpms` passes a list object of extra RPMs to the built image.

   These options are passed to `osbuild-mpp` as separate `-D` flags.
   Each of these options are individually processed as JSON input, and therefore quotes must be escaped or nested correctly at the command line, for example:

   ```console
   osbuild-mpp images/minimal.mpp.yml /tmp/output.json -I . -D image_type=\"regular\" -D extra_rpms=[\"dnf\",\"vim-enhanced\"]
   ```

   or

   ```console
   osbuild-mpp images/minimal.mpp.yml /tmp/output.json -I . -D 'image_type="regular"' -D 'extra_rpms=["dnf","vim-enhanced"]'
   ```

2. After preprocessing, pass the resolved manifest to `osbuild`, which builds the image in a series of steps:

   ```console
   sudo osbuild /tmp/output.json --store store/ --output-directory out/ --export qcow2
   ```

   When you run `osbuild` you can choose which steps to export, typically either _image_ or _qcow2_:

   - Image is a raw image that can be written to disk.
   - QCOW2 is a format used by QEMU for image files.

### Using makefile to build the image

Simplify the build process by using makefile.

!!! note

    Makefile uses sudo, so you might be asked for your password during the build process.

**Procedure**

1. From the checkout directory, run `make`, using the correct image name as the target:

    ```console
    make cs9-qemu-minimal-regular.x86_64.qcow2
    ```

    This command preprocesses and builds the manifest for the current architecture and defines the image type to be `ostree`.
    This results in a file named `cs9-qemu-minimal-regular.x86_64.qcow2`, which is stored in the current directory.

!!! note

    You can use `make` to build RAW images as well. For a full list of images types available for the current architecture, run `make help`.

2. Optional: Remove the `_build` directory to regain disk space:

    ```
    rm -r _build
    ```

!!! note

    During the build process, artifacts such as JSON files, RPMs, and cached parts from previous builds are stored in the `_build` directory.
    Everything in this directory is derived from other sources.

### Changing the default password

The sample images use `password` as the default password for `root` and `guest`.
For security, you should change the default password.

**Procedure**

- Change the default password when you build the image:

```console
DEFINES='root_password="<hash_of_new_password>"'
```

## Running the image

You can either boot the image in QEMU/KVM or flash the image onto an SD card.

### Booting the image in QEMU/KVM

**Procedure**

- Boot the QCOW2 image in `virt-manager` or run it directly through QEMU using `runvm`:

```console
./runvm cs9-qemu-minimal-regular.x86_64.qcow2
```

!!! note

    On MacOS, you must install `qemu` to use `runvm`:

    ```
    brew install qemu
    ```

### Flashing the image on an SD card

- Flash the image on an SD card:

```console
dd if=cs9-qemu-minimal-regular.x86_64.img of=<SD card device path> status=progress bs=4M
```

!!! important

    You must change the value for `of=` to match the block device path used in your system, for example `/dev/sdb`. If required, add `-bios /usr/share/OVMF/OVMF_CODE.fd` to QEMU command.

## Using make manifests

<!--Could this section join the Using makefile to build the image section? It seems organizationally related.-->

Use `make manifests` to preprocess all existing manifests without building them.
You can use `make manifests` to verify that all combinations of options still work after a change or to inspect the resulting manifests.

In addition to the image types `*.img` and `*.qcow2`, the makefile also supports targets such as:

- `*.rootfs`
- `*.repo`
- `*.tar`
- `*.container`
- `*.ext4`

These targets are useful during development and testing. For more information, run `make help`.

## Building an image in a VM

The makefile configuration supports running osbuild inside a VM. There are two scenarios in which this is beneficial:

- Standard osbuild requires root/sudo permissions, because it performs some system-level operations such as loopback mounts.
  Running osbuild in a VM allows you to run it non-privileged.
- When you build an image for a different architecture, you can use QEMU software emulation to make this work.
  Software emulation is slower than native, but for some use cases it is sufficient.

Running `make osbuildvm-images` uses osbuild on the host to build the supporting image files, `_build/osbuildvm-*`.
These files are required to build an image in a VM.
The supporting image files can later be used to build other images from the same architecture inside a VM by passing `VM=1` to `make`.

To build images from a different architecture in a VM, there are two options:

- Copy the output of `make osbuildvm-images` from a build on a different architecture into `_build`.
  For example, the contents of _build/osbuildvm-* from an aarch64 machine into `_build/` on an x86_64 machine.

- Use pre-built osbuildvm images. To build for aarch64 on an x86_64 host, follow the steps outlined below.

  Change to the sample-images directory

  ```console
  cd sample-images/
  ```

  Retrieve the nightly osbuildvm* files and place them into the `_build` folder

  ```console
  wget -r -l 1 -nH -nd -np -A 'osbuildvm*' -P _build https://autosd.sig.centos.org/AutoSD-9/nightly/osbuildvm-images/
  ```

After the `_build` folder is populated with the relevant `osbuildvm-*` files, you may then run `make` with a target that has
an architecture different to the host system, for example, make `cs9-qemu-minimal-ostree.aarch64.qcow2`.

!!! note

     You can download pre-built osbuildvm-images files for the aarch64 architecture from the nightly folder at: https://autosd.sig.centos.org/AutoSD-9/nightly/osbuildvm-images/

!!! note

     If you have a previous version of `osbuildvm-images` for a different architecture, rebuild or refresh them by using `VM=1` on your architecture.

## Additional resources

For more information, see:

- [Updating an OSTree-based image](updating_ostree.md)
- [Customizing your OSBuild template](customize_template.md)
- [Embedding Containers in images](containers.md)
- [Encrypted images](encryption.md)
- [Using a Raspberry Pi as USB gadget](gadget.md)
- [Boot Time Optimization](boot_time_optimization.md)
- [Automotive Communities](automotive_communities.md)
