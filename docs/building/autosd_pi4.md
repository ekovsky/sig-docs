# Running AutoSD on Raspberry pi 4

The Automotive SIG publishes images built via [OSBuild](https://www.osbuild.org/)
based on the OSBuild manifest present in the [sample-images](https://gitlab.com/CentOS/automotive/sample-images/)
repository.

Here is a quick guide on how to get you started with them.

We recommend you download the `gadget` image to start with (see our
[Download images](../download_images.md) page for more information about the
different images built).

1. Update eeprom.

    !!! note

        If eeprom is out-of-date, you might experience boot failures on Raspberry Pi 4 (rpi4).
        For more information about how to update eeprom, [see the guide](https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#rpi-eeprom-update).

1. Download your rpi4 image from [https://autosd.sig.centos.org/AutoSD-9/nightly/](https://autosd.sig.centos.org/AutoSD-9/nightly/).
1. Unpack the image.

    ```console
    unxz auto-osbuild-rpi4-cs9-gadget-regular-aarch64-*.raw.xz
    ```

1. Flash it onto your SD card.

    !!! important

        Change the block device, shown as _`/dev/disk0`_ in the example, according to your system.


         ```console
         sudo dd if=auto-osbuild-rpi4-cs9-gadget-regular-aarch64-*.raw of=/dev/disk0 status=progress bs=4M; sync
         ```

1. Insert the SD card into your Raspberry Pi 4.
1. Boot the Raspberry Pi 4.
1. Connect the Raspberry Pi 4.

    !!! important

        Serial and WiFi are not yet functional.

        Connect to the Raspberry Pi 4 via a USB keyboard or via SSH with an Ethernet cable.

1. Log in as `root` or `guest` using the password: `password`.
1. Connect the Raspberry Pi 4 to the internet.

     * Option 1)
       Connect by using an Ethernet cable if you have one. This is the most simple solution.
     * Option 2)
       Enable the WiFi adapter on the Raspberry Pi 4:

        ```console
        # dnf install -y NetworkManager-wifi
        # systemctl restart NetworkManager
        ```

        List the surrounding WiFi SSIDs and connect to them:

        ```console
        # nmcli dev wifi list
        # nmcli --ask dev wifi connect <some-wifi-ssid>
        ```

     * Option 3)
       If you are not near a router or cannot connect the Raspberry Pi by using an Ethernet cable,
       use the `gadget` image. For more information about how to use the `gadget` image,
       see [USB gadget](gadget.md).

## Enable Bluetooth

1. To enable Bluetooth functionality, install some required packages:

    ```console
    # dnf install NetworkManager-bluetooth
    # systemctl restart NetworkManager
    ```

1. Enable and start the service:

    ```console
    # systemctl enable --now bluetooth
    ```

1. List the available Bluetooth devices:

    ```console
    # bluetoothctl list
    Controller AA:AA:AA:AA:AA:AA BlueZ 5.56 [default]
    ```

    Notice the MAC address of the device is listed as `AA:AA:AA:AA:AA:AA`. This is incorrect, and it indicates that some Linux firmware is missing.

    ```console
    # cd lib
    # grep -rni . -e firmware/ | grep brcm | grep dracut
    ./dracut/modules.d/62bluetooth/module-setup.sh:37:        /lib/firmware/brcm/*.hcd* \

    # ls /lib/firmware/brcm/
    'brcmfmac43430a0-sdio.ONDA-V80 PLUS.txt.xz'  'brcmfmac43455-sdio.MINIX-NEO Z83-4.txt.xz'            'brcmfmac43455-sdio.Raspberry Pi Foundation-Raspberry Pi 4 Model B.txt.xz'
    brcmfmac43455-sdio.acepc-t8.txt.xz           brcmfmac43455-sdio.raspberrypi,3-model-a-plus.txt.xz  'brcmfmac43455-sdio.Raspberry Pi Foundation-Raspberry Pi Compute Module 4.txt.xz'
    brcmfmac43455-sdio.bin.xz                    brcmfmac43455-sdio.raspberrypi,3-model-b-plus.txt.xz
    brcmfmac43455-sdio.clm_blob.xz               brcmfmac43455-sdio.raspberrypi,4-model-b.txt.xz
    ```

    Notice there are no *.hcd files for any Bluetooth firmware.

1. Download the required firmware from GitHub:

    ```console
    # cd ~
    # git clone https://github.com/RPi-Distro/bluez-firmware/tree/master/broadcom
    # cd bluez-firmware/broadcom
    # cp *.hcd /lib/firmware/brcm/
    ```

1. Reboot the system:

    ```console
    # reboot
    ```

1. List the available Bluetooth devices again to confirm that the device has a MAC address:

    ```console
    # bluetoothctl list
    Controller DC:A6:32:B9:6A:DC BlueZ 5.56 [default]
    ```

1. Start the device:

    ```console
    # bluetoothctl
    > power on
    Changing power on succeeded.
    ```

1. Put a selected device, such as a controller or a portable speaker, in pairing mode so you can connect it to the Raspberry Pi 4.

1. Scan for nearby Bluetooth devices:

    ```console
    > scan on
    ...
    [NEW] Device F4:93:9F:63:7F:6C Wireless Controller
    ...
    ```

1. Connect to the device:

    ```console
    > connect F4:93:9F:63:7F:6C

    [bluetooth]# connect F4:93:9F:63:7F:6C
    Attempting to connect to F4:93:9F:63:7F:6C
    [CHG] Device F4:93:9F:63:7F:6C Connected: yes
    [CHG] Device F4:93:9F:63:7F:6C UUIDs: 00001124-0000-1000-8000-00805f9b34fb
    [CHG] Device F4:93:9F:63:7F:6C UUIDs: 00001200-0000-1000-8000-00805f9b34fb
    [CHG] Device F4:93:9F:63:7F:6C ServicesResolved: yes
    [CHG] Device F4:93:9F:63:7F:6C Paired: yes
    Connection successful
    ```

1. After the device is connected, exit `bluetoothctl`:

    ```console
    > exit
    ```
